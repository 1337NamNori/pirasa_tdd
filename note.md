# What?
- Create the test first, then the code.
- Write and correct the failed tests before writing new code
# How?
![img_1.png](img_1.png)
# Advantages 
- Early bug notification.
- Easy to reach 100% test coverage
- Confidence to Refactor
- Good for teamwork.
- More time writing TDD test case, less time coding and debugging.
- Minimum the code. (KISS, YAGNI)
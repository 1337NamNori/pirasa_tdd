<?php

namespace App;

class FizzBuzz
{
    private function isFizz($number)
    {
        return $number % 3 === 0;
    }

    private function isBuzz($number)
    {
        return $number % 5 === 0;
    }

    public function convert($number)
    {
        if ($this->isFizz($number) && $this->isBuzz($number)) {
            return 'FizzBuzz';
        }
        if ($this->isFizz($number)) {
            return 'Fizz';
        }
        if ($this->isBuzz($number)) {
            return 'Buzz';
        }
        return $number;
    }
}
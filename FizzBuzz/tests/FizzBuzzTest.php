<?php

namespace Test;

use PHPUnit\Framework\TestCase;
use App\FizzBuzz;

class FizzBuzzTest extends TestCase
{
    public function data_input_provider(): array
    {
        return [
            'Number 1 should returns 1' => [1, 1],
            'Number 4 should returns 4' => [4, 4],
            'Number 3 is divisible by 3 should returns Fizz' => [3, 'Fizz'],
            'Number 6 is divisible by 3 should returns Fizz' => [6, 'Fizz'],
            'Number 5 is divisible by 5 should returns Buzz' => [5, 'Buzz'],
            'Number 10 is divisible by 5 should returns Buzz' => [10, 'Buzz'],
            'Number 15 is divisible by 3 and 5 should returns FizzBuzz' => [15, 'FizzBuzz'],
            'Number 30 is divisible by 3 and 5 should returns FizzBuzz' => [30, 'FizzBuzz'],
        ];
    }

    /**
     * @dataProvider data_input_provider
     */
    public function test_fizz_buzz($input, $expected)
    {
        $fizzBuzz = new FizzBuzz();
        $result = $fizzBuzz->convert($input);

        $this->assertEquals($expected, $result);
    }
}

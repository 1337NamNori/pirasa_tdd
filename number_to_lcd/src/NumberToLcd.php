<?php

namespace App;

use InvalidArgumentException;

class NumberToLcd
{
    private int $number;
    private const LCD_NUMBERS = [
        0 => LCD_ZERO,
        1 => LCD_ONE,
        2 => LCD_TWO,
        3 => LCD_THREE,
        4 => LCD_FOUR,
        5 => LCD_FIVE,
        6 => LCD_SIX,
        7 => LCD_SEVEN,
        8 => LCD_EIGHT,
        9 => LCD_NINE,
    ];

    public function __construct(int $number)
    {
        $this->number = $number;
    }

    public function convert(int $width = 1, int $height = 1): string
    {
        if ($height <= 0 || $width <= 0) {
            throw new InvalidArgumentException('Height and width must be greater than 0');
        }
        $digits = $this->splitNumberToDigitsArray();
        $linesArrays = $this->splitDigitsToLinesArrays($digits, $width, $height);

        $lcd = '';
        for ($i = 0; $i < sizeof($linesArrays[0]); $i++) {
            if (!empty($lcd)) {
                $lcd .= PHP_EOL;
            }

            $lcd .= $this->concatLinesArrays($linesArrays, $i);
        }

        return $lcd;
    }

    public function splitNumberToDigitsArray(): array
    {
        $strNumber = (string) $this->number;
        return str_split($strNumber);
    }

    public function splitDigitToLinesArray(int $digit, int $width, int $height): array
    {
        $displayedDigits = self::LCD_NUMBERS[$digit];
        $lines = mb_split(PHP_EOL, $displayedDigits);

        $firstBaseLine = $lines[0];
        $secondBaseLine = $lines[1];
        $thirdBaseLine = $lines[2];

        $scaledLines[] = $firstBaseLine[0] . str_pad('', $width, $firstBaseLine[1]) . $firstBaseLine[2];

        for ($i=0; $i < $height - 1; $i++) {
            $scaledLines[] = $secondBaseLine[0] . str_pad('', $width, ' ') . $secondBaseLine[2];
        }
        $scaledLines[] = $secondBaseLine[0] . str_pad('', $width, $secondBaseLine[1]) . $secondBaseLine[2];

        for ($i = 0; $i < $height - 1; $i++) {
            $scaledLines[] = $thirdBaseLine[0] . str_pad('', $width, ' ') . $thirdBaseLine[2];
        }
        $scaledLines[] = $thirdBaseLine[0] . str_pad('', $width, $thirdBaseLine[1]) . $thirdBaseLine[2];

        return $scaledLines;
    }

    public function splitDigitsToLinesArrays(array $digits, int $width, int $height): array
    {
        $linesArrays = [];
        foreach ($digits as $digit) {
            $linesArray = $this->splitDigitToLinesArray((int) $digit, $width, $height);
            $linesArrays[] = $linesArray;
        }

        return $linesArrays;
    }

    public function concatLinesArrays(array $linesArrays, int $lineToConcat): string
    {
        $concatLine = '';
        foreach ($linesArrays as $linesArray) {
            if (!empty($concatLine)) {
                $concatLine .= ' ';
            }
            $concatLine .= $linesArray[$lineToConcat];
        }

        return $concatLine;
    }
}
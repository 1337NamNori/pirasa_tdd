<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\NumberToLcd;

class NumberToLcdTest extends TestCase
{
    public function dataInputProvider(): array
    {
        return [
            'Number 0' => [0, LCD_ZERO],
            'Number 1' => [1, LCD_ONE],
            'Number 2' => [2, LCD_TWO],
            'Number 3' => [3, LCD_THREE],
            'Number 4' => [4, LCD_FOUR],
            'Number 5' => [5, LCD_FIVE],
            'Number 6' => [6, LCD_SIX],
            'Number 7' => [7, LCD_SEVEN],
            'Number 8' => [8, LCD_EIGHT],
            'Number 9' => [9, LCD_NINE],
            'Number 214' => [214, <<<NUMBER
             _         
             _|   | |_|
            |_    |   |
            NUMBER],
            'Number 1234567890' => [1234567890, <<<NUMBER
                 _   _       _   _   _   _   _   _ 
              |  _|  _| |_| |_  |_    | |_| |_| | |
              | |_   _|   |  _| |_|   | |_|  _| |_|
            NUMBER],
        ];
    }

    /**
     * @test
     * @covers NumberToLcd::convert
     * @dataProvider dataInputProvider
     */
    public function number_to_lcd_test($input, $expected)
    {
        $numberToLcd = new NumberToLcd($input);
        $result = $numberToLcd->convert();

        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     * @covers NumberToLcd::convert
     */
    public function number_2_to_lcd_width_2_height_3_test()
    {
        $numberToLcd = new NumberToLcd(2);
        $result = $numberToLcd->convert(2, 3);

        $this->assertEquals(<<<NUMBER
         __ 
           |
           |
         __|
        |   
        |   
        |__ 
        NUMBER, $result);
    }

    /**
     * @test
     * @covers NumberToLcd::convert
     */
    public function number_214_to_lcd_width_4_height_3_test()
    {
        $numberToLcd = new NumberToLcd(214);
        $result = $numberToLcd->convert(4, 3);

        $this->assertEquals(<<<NUMBER
         ____               
             |      | |    |
             |      | |    |
         ____|      | |____|
        |           |      |
        |           |      |
        |____       |      |
        NUMBER, $result);
    }

    /**
     * @test
     * @covers NumberToLcd::convert
     */
    public function throw_error_when_height_is_less_than_1_test()
    {
        $this->expectException(\InvalidArgumentException::class);
        $numberToLcd = new NumberToLcd(214);
        $numberToLcd->convert(4, 0);
    }

    /**
     * @test
     * @covers NumberToLcd::convert
     */
    public function throw_error_when_width_is_less_than_1_test()
    {
        $this->expectException(\InvalidArgumentException::class);
        $numberToLcd = new NumberToLcd(214);
        $numberToLcd->convert(0, 3);
    }

    /**
     * @test
     * @covers NumberToLcd::convert
     */
    public function throw_error_when_input_is_not_numeric_test()
    {
        $this->expectException(\TypeError::class);
        $numberToLcd = new NumberToLcd('abc');
        $numberToLcd->convert();
    }
}
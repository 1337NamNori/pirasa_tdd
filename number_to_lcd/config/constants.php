<?php
if (!defined('LCD_ZERO')) define('LCD_ZERO', <<<NUMBER
 _ 
| |
|_|
NUMBER);
if (!defined('LCD_ONE')) define('LCD_ONE', <<<NUMBER
   
  |
  |
NUMBER);
if (!defined('LCD_TWO')) define('LCD_TWO', <<<NUMBER
 _ 
 _|
|_ 
NUMBER);
if (!defined('LCD_THREE')) define('LCD_THREE', <<<NUMBER
 _ 
 _|
 _|
NUMBER);
if (!defined('LCD_FOUR')) define('LCD_FOUR', <<<NUMBER
   
|_|
  |
NUMBER);
if (!defined('LCD_FIVE')) define('LCD_FIVE', <<<NUMBER
 _ 
|_ 
 _|
NUMBER);
if (!defined('LCD_SIX')) define('LCD_SIX', <<<NUMBER
 _ 
|_ 
|_|
NUMBER);
if (!defined('LCD_SEVEN')) define('LCD_SEVEN', <<<NUMBER
 _ 
  |
  |
NUMBER);
if (!defined('LCD_EIGHT')) define('LCD_EIGHT', <<<NUMBER
 _ 
|_|
|_|
NUMBER);
if (!defined('LCD_NINE')) define('LCD_NINE', <<<NUMBER
 _ 
|_|
 _|
NUMBER);
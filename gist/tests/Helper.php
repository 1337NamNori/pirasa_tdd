<?php

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

function getClientWithResponse(array $response): Client
{
    $mock = new MockHandler($response);

    $handler = HandlerStack::create($mock);
    return new Client(['handler' => $handler]);
}
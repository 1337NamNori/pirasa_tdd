<?php

namespace App\Dto;

class Gist
{
    private string $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }
    
    public function getUrl()
    {
        return $this->url;
    }
}
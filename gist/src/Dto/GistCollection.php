<?php

namespace App\Dto;

use ArrayObject;

class GistCollection extends ArrayObject
{
    public function __construct(array $gists = [])
    {
        foreach($gists as $gist) {
            $this->append(new Gist($gist->url));
        }
    }
}
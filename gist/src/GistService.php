<?php

namespace App;

use App\Dto\GistCollection;
use App\Exception\UserNotFoundException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class GistService
{
    private string $username;
    private Client $client;

    public function __construct(string $username, Client $client = null)
    {
        $this->username = $username;
        $this->client = $client ?? new Client();
    }

    private function getUrl(): string
    {
        return 'https://api.github.com/users/' . $this->username . '/gists';
    }

    public function getAll(): GistCollection
    {
        $url = $this->geturl();
        try {
            $response = $this->client->get($url);
        } catch (ClientException $e) {
            throw new UserNotFoundException();
        }

        $responseDecoded = json_decode($response->getBody());

        return new GistCollection($responseDecoded);
    }
}